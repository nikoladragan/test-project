(function() {
 
  // store the slider in a local variable
  var $window = $(window),
      flexslider;
 
  // tiny helper function to add breakpoints
  function getGridSize() {
    return (window.innerWidth < 600) ? 2 :
           (window.innerWidth < 900) ? 3 : 4;
  }
 
  /*$(function() {
    SyntaxHighlighter.all();
  });*/
 
  $window.load(function() {
    $('.flexslider').flexslider({      
        animation: "slide",
        animationLoop: false,
        itemWidth: 290,
        itemMargin: 0,
        prevText: " ",
        nextText: " ",
        minItems: getGridSize(),
        maxItems: getGridSize(),
        start: function (slider) {
            flexslider = slider;
        }// use function to pull in initial value
    });
  });
 
  // check grid size on resize event
  $window.resize(function() {
    var gridSize = getGridSize();
 
    flexslider.vars.minItems = gridSize;
    flexslider.vars.maxItems = gridSize;
  });
}());